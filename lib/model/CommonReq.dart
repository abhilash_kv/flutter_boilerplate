class CommonReq {
    String message;
    int success;

    CommonReq({required this.message, required this.success});

    factory CommonReq.fromJson(Map<String, dynamic> json) {
        return CommonReq(
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        return data;
    }
}