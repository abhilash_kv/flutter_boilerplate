class CommonRes {
    String message;
    int success;

    CommonRes({required this.message, required this.success});

    factory CommonRes.fromJson(Map<String, dynamic> json) {
        return CommonRes(
            message: json['message'], 
            success: json['success'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['message'] = this.message;
        data['success'] = this.success;
        return data;
    }
}