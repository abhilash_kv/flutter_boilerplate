import 'package:dio/dio.dart';
import 'package:flutter_boilerplate/home/HomeProvider.dart';
import 'package:flutter_boilerplate/home/HomeRepo.dart';
import 'package:flutter_boilerplate/utils/NavigationService.dart';
import 'package:flutter_boilerplate/values/values.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dio/dio_client.dart';
import 'dio/logging_interceptor.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => LoggingInterceptor());
  sl.registerLazySingleton(() => NavigationService());

  sl.registerLazySingleton(
      () => DioClient(StringConst.BASE_URL, sl(), loggingInterceptor: sl(), sharedPreferences: sl(), navigationService: sl()));

  // Repository
  sl.registerLazySingleton(() => HomeRepo(sharedPreferences: sl(), dioClient: sl()));

  // Provider
  sl.registerFactory(() => HomeProvider(splashRepo: sl()));
}
