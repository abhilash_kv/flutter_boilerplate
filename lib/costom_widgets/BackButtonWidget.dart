import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';


class BackButtonWidget extends StatelessWidget {
  final title;

  const BackButtonWidget({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
                margin: const EdgeInsets.only(left: 24.0),
                padding: const EdgeInsets.only(left: 48.0, right: 16.0),
                height: 40.0,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(23.0), color: AppColors.blue),
                child: Center(widthFactor: 1.0, child: Text(title, style: TextStyles.black_20_700(), textAlign: TextAlign.center)))),
        RawMaterialButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          elevation: 2.0,
          fillColor: AppColors.black,
          child: Icon(Icons.arrow_back_ios_rounded, color: AppColors.blue, size: 24.0),
          padding: EdgeInsets.all(10.0),
          shape: CircleBorder(),
        )
      ],
    );
  }
}
