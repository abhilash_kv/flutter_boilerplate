import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomTextFormFieldDecimal extends StatelessWidget {
  final String hint;
  final TextEditingController? controller;
  final int? maxLength;
  final String? initialValue;

  CustomTextFormFieldDecimal({required this.hint, this.controller, this.maxLength,this.initialValue});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialValue,
      maxLength: maxLength,
      controller: controller,
      style: TextStyle(fontSize: 18.0, color: AppColors.white),
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
        TextInputFormatter.withFunction((oldValue, newValue) {
          try {
            final text = newValue.text;
            if (text.isNotEmpty) double.parse(text);
            return newValue;
          } catch (e) {}
          return oldValue;
        }),
      ],
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        counterText: "",
        contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
        labelText: hint,
        labelStyle: TextStyle(fontSize: 18.0, color: AppColors.hint2),
        focusedBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        border: OutlineInputBorder(),
      ),
    );
  }
}
