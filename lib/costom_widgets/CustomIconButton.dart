import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomIconButton extends StatelessWidget {
  final String? svgPath;
  final IconData? icon;
  final VoidCallback onPressed;

  const CustomIconButton({Key? key, this.svgPath, this.icon, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: IconButton(
          color: AppColors.white,
          splashColor: AppColors.primaryColor,
          splashRadius: 24.0,
          icon: svgPath != null
              ? SvgPicture.asset(svgPath!, color: AppColors.white, width: 24.0, height: 24.0)
              : icon != null
                  ? Icon(icon, size: 32.0)
                  : Icon(Icons.info_outline),
          onPressed: () {
            onPressed();
          }),
    );
  }
}
