import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomButtonBlueWrapWidth extends StatelessWidget {
  final String title;
  final Function onPressed;

  const CustomButtonBlueWrapWidth({Key? key, required this.title, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0))),
            elevation: MaterialStateProperty.all(2.0)),
        onPressed: () {
          onPressed();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            title,
            style: TextStyles.textField1(),
          ),
        ),
      ),
    );
  }
}
