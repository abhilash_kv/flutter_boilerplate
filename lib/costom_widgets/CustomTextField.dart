import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;

  const CustomTextField({Key? key, required this.controller, required this.hintText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      maxLines: 1,
      controller: controller,
      autofocus: false,
      style: TextStyles.textField1(),
      decoration: Decorations.textFieldDecoration1(hintText),
    );
  }
}
