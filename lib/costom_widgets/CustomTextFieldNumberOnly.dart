import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomTextFieldNumberOnly extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;

  const CustomTextFieldNumberOnly({Key? key, required this.controller, required this.hintText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      maxLines: 1,
      maxLength: 4,
      autofocus: false,
      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
      keyboardType: TextInputType.number,
      style: TextStyles.textField1(),
      decoration: Decorations.textFieldDecoration1(hintText),
    );
  }
}
