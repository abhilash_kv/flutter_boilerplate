import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomRippleContainerWhite extends StatelessWidget {
  final Function  onTap;
  final Widget child;
  final Color? color;

  const CustomRippleContainerWhite({Key? key,required this.onTap, required this.child, this.color = AppColors.white}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      child: InkWell(
        onTap: () {
          onTap();
        },
        splashColor: AppColors.hint,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
          child: child,
        ),
      ),
    );
  }
}
