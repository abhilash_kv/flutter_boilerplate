import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomButtonBlue extends StatelessWidget {
  final String title;
  final Function onPressed;

  const CustomButtonBlue({Key? key, required this.title, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 48.0,
      child: ElevatedButton(
        style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0))),
            elevation: MaterialStateProperty.all(2.0)),
        onPressed: () {
          onPressed();
        },
        child: Text(
          title,
          style: TextStyles.textField1(),
        ),
      ),
    );
  }
}
