import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomTextFormField extends StatelessWidget {
  final String hint;
  final TextEditingController? controller;
  final FocusNode? focusNode;

  CustomTextFormField({required this.hint, this.controller,this.focusNode});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.next,
      autofocus: false,
      focusNode: focusNode,
      controller: controller,
      style: TextStyle(fontSize: 18.0, color: AppColors.white),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
        labelText: hint,
        labelStyle: TextStyle(fontSize: 18.0, color: AppColors.hint2),
        focusedBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white, width: 2.0),
        ),
        border: OutlineInputBorder(),
      ),
    );
  }
}
