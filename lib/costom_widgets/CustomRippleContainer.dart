import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/values/values.dart';

class CustomRippleContainer extends StatelessWidget {
  final Widget child;
  final Color? color;
  final Function onTap;

  const CustomRippleContainer({Key? key, required this.child, this.color = AppColors.primaryColorDark,required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(8.0),
      color: AppColors.primaryColorDark,
      child: InkWell(
        onTap: () {
          onTap();
        },
        borderRadius: BorderRadius.circular(8.0),
        splashColor: AppColors.hint,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0)),
          child: child,
        ),
      ),
    );
  }
}
