
import 'package:flutter_boilerplate/dio/dio_client.dart';
import 'package:flutter_boilerplate/model/CommonRes.dart';
import 'package:flutter_boilerplate/values/values.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeRepo {
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;


  HomeRepo({required this.dioClient, required this.sharedPreferences});

  Future<CommonRes> getVersion() async {
    print(dioClient.baseUrl);
    try {
      final response = await dioClient.post(StringConst.API_VERSION);
      return CommonRes(message: "",success: 1);
    } catch (e) {
      print(e);
      return CommonRes.fromJson({});
    }
  }
}
