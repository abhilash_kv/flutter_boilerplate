import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/costom_widgets/CustomImageButtonBlue.dart';
import 'package:flutter_boilerplate/home/HomeProvider.dart';
import 'package:flutter_boilerplate/utils/NavigationService.dart';
import 'package:flutter_boilerplate/values/Dialogs.dart';
import 'package:flutter_boilerplate/values/values.dart';
import 'package:provider/provider.dart';

import '../BaseWidget.dart';
import 'package:flutter_boilerplate/locator.dart' as di;

import 'Home2.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String text = "AAAA";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Home build");
    /* WidgetsBinding.instance!.addPostFrameCallback((_) {
      Dialogs.showLoading(context: context);
    });*/

    return BaseWidget(child: Consumer<HomeProvider>(
      builder: (context, provider, child) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                provider.text,
                style: TextStyles.white_20_700(),
              ),
              SizedBox(height: 16.0),
              CustomButtonBlue(
                  title: "Sample Button",
                  onPressed: () {
                    provider.changeText("clicked");
                    Alerts.showSuccess(provider.text);
                   Navigator.push(context, MaterialPageRoute(builder: (context) => Home2()));
                  }),
              TextButton(
                style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double>(2.0),
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.amber),
                  padding: MaterialStateProperty.all(EdgeInsets.symmetric(horizontal: 16.0)),
                ),
                onPressed: () {},
                child: Text("sss"),
              )
            ],
          ),
        );
      },
    ));
  }
}
