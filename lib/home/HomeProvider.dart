import 'package:flutter/foundation.dart';
import 'package:flutter_boilerplate/home/HomeRepo.dart';
import 'package:flutter_boilerplate/model/CommonRes.dart';

class HomeProvider with ChangeNotifier {
  final HomeRepo splashRepo;
  String text = "Welcome";

  HomeProvider({required this.splashRepo});

  Future<CommonRes> getVersion() async {
    var a = await splashRepo.getVersion();
    return a;
  }

  void changeText(String s) {
    text = s;
    notifyListeners();
  }
}
