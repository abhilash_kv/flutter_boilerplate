import 'package:flutter/cupertino.dart';
import 'package:flutter_boilerplate/costom_widgets/CustomButtonBlue.dart';
import 'package:flutter_boilerplate/home/HomeProvider.dart';
import 'package:flutter_boilerplate/utils/NavigationService.dart';
import 'package:provider/provider.dart';

import '../BaseWidget.dart';
import 'package:flutter_boilerplate/locator.dart' as di;

class Home2 extends StatelessWidget {
  const Home2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(child: Consumer<HomeProvider>(builder: (context, provider, child) {
      return CustomButtonBlue(
          title: "Sample Button",
          onPressed: () async {
            var data = await provider.getVersion();
          });
    }));
  }
}
