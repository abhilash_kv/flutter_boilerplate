library values;

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:overlay_support/overlay_support.dart';




part 'AppColors.dart';
part 'StringConst.dart';
part 'TextStyles.dart';
part 'Sizes.dart';
part 'ImagePath.dart';
part 'Decorations.dart';
part 'Alerts.dart';