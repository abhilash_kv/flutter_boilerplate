part of values;

class Alerts {
  static showError(String text) {
    return showSimpleNotification(Text(text), background: Colors.redAccent);
  }

  static showSuccess(String text) {
    return showSimpleNotification(Text(text), background: Colors.green);
  }
  static show(int type, String text) {
    if(type ==1)
      {
        showSuccess(text);
      }else{
      showError(text);
    }
  }
}
