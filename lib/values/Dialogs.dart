import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Dialogs {
  static showLoading({
    required BuildContext context,
  }) {
    showDialog(
      barrierColor: Colors.black.withOpacity(0.7),
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Dialog(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: Lottie.asset('assets/lottie/loading.json', width: 60.0, fit: BoxFit.cover),
          ),
        );
      },
    );
  }
}
