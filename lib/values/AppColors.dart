part of values;



class AppColors {
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color red = Color(0xFFFF1100);
  static const Color green = Color(0xFF349600);
  static const Color primaryColor = Color(0xFF4A4E69);
  static const Color primaryColorDark = Color(0xFF22223b);
  // static const Color blue = Color(0xFF1366ca);
  static const Color blue = Color(0xFF0A8CF3);
  static const Color hint = Color(0xFF6f7071);
  static const Color divider = Color(0xFFDBDBDB);
  static const Color hint2 = Color(0xFFADADAD);

  static const Color gradientStart = Color(0xFFdfe8ff);
  static const Color gradientEnd = Color(0xFF94b4ff);

}