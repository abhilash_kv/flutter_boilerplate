part of values;

class ImagePath {
  //images route
  static const String imageDir = "assets/images";
  static const String iconDir = "assets/icons";

  static const String LOGO = "$imageDir/logo.png";

  //
  static const String ICON_AVATAR_PNG = "$iconDir/avatar.png";
}
