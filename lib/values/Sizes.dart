part of values;

class Sizes {
  static const double TEXT_SIZE_16 = 16.0;
  static const double TEXT_SIZE_18 = 18.0;
  static const double TEXT_SIZE_20 = 20.0;
  static const double TEXT_SIZE_22 = 22.0;
  static const double TEXT_SIZE_24 = 24.0;
  }