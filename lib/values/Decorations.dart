part of 'values.dart';

class Decorations {
  static BoxDecoration boxDecorationColorBorder({Color color = const Color(0xFFFFFFFF), borderRadius = 4.0}) {
    return BoxDecoration(color: color, borderRadius: BorderRadius.all(Radius.circular(borderRadius)));
  }

  static BoxDecoration boxDecorationGradient() {
    return BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[AppColors.gradientStart, AppColors.gradientEnd],
        ),
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.white, width: 3.0));
  }

  static InputDecoration textFieldDecoration1(String hintText) {
    return InputDecoration(
      counterText: "",
      filled: true,
      isDense: true,
      alignLabelWithHint: false,
      fillColor: AppColors.primaryColorDark,
      hintText: hintText,
      hintStyle: TextStyle(fontSize: 16.0, color: AppColors.hint, fontFamily: 'Roboto'),
      contentPadding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0), borderSide: BorderSide(color: AppColors.primaryColorDark)),
      enabledBorder: UnderlineInputBorder(borderRadius: BorderRadius.circular(8.0), borderSide: BorderSide(color: AppColors.primaryColorDark)),
    );
  }
}
