import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/home/HomeProvider.dart';
import 'package:flutter_boilerplate/utils/NavigationService.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'locator.dart' as di;
import 'home/Home.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => di.sl<HomeProvider>()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OverlaySupport.global(
      child: MaterialApp(
        navigatorKey: di.sl<NavigationService>().navigatorKey,
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Home(),
      ),
    );
  }
}
