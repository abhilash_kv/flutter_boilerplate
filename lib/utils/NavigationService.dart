import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  Future<dynamic> navigateTo(Widget widget) {
    return  Navigator.pushAndRemoveUntil(
        navigatorKey.currentState!.context, MaterialPageRoute(builder: (context) => widget), (Route<dynamic> route) => false);
  }
}
